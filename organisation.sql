CREATE TABLE `organisation` (
  `id` int(10) NOT NULL,
  `name` varchar(500) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `organisation` (`id`, `name`) VALUES
(834, 'Banana tree'),
(839, 'Big banana tree'),
(837, 'Black Banana'),
(836, 'Brown Banana'),
(838, 'Green Banana'),
(833, 'Paradise Island'),
(843, 'Phoneutria Spider'),
(835, 'Yellow Banana');


CREATE TABLE `relations` (
  `id` int(10) NOT NULL,
  `name` varchar(500) CHARACTER SET utf8 NOT NULL,
  `parent` varchar(500) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



INSERT INTO `relations` (`id`, `name`, `parent`) VALUES
(812, 'Banana tree', 'Paradise Island'),
(811, 'Big banana tree', 'Paradise Island'),
(814, 'Black Banana', 'Banana tree'),
(818, 'Black Banana', 'Big banana tree'),
(815, 'Brown Banana', 'Banana tree'),
(817, 'Brown Banana', 'Big banana tree'),
(819, 'Green Banana', 'Big banana tree'),
(820, 'Phoneutria Spider', 'Black Banana'),
(813, 'Yellow Banana', 'Banana tree'),
(816, 'Yellow Banana', 'Big banana tree');


ALTER TABLE `organisation`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

ALTER TABLE `relations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_name_parent` (`name`,`parent`) USING BTREE;

ALTER TABLE `organisation`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=844;

ALTER TABLE `relations`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=841;COMMIT;