var express = require('express');
var bodyParser = require('body-parser');
// var cors = require("cors");
var app = express();

app.use(bodyParser.json())


// app.use(cors());
app.use('/api', require('./routes/organisations'));
app.set('view engine', 'pug');

app.listen(3000);
console.log('API is running on port 3000');