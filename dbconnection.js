var mysql = require('mysql');
var connection = mysql.createPool({
    host: 'localhost',
    database: 'sampledb',
    user: 'root',
    password: ''
});

connection.getConnection(function (err, tempCon) {
    if (err) {
        console.log('Error connecting to Dbs');
        return;
    }
    console.log('Connection established');
});

module.exports = connection;