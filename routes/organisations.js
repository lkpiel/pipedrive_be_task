var express = require('express');
var router = express.Router();
var Organisation = require('../models/organisation');


//DEFAULT HOME
router.get('/', function(req, res, next) {
  res.render('index');
});

//POST NEW ORGANISATION
router.post('/organisations', function (req, res ) {
  Organisation.addOrganisation(req.body);
  //Not actually needed.
  res.send("Adding organisation");
});

//GET RELATIONS BY ORGANISATION NAME
router.get('/organisations', function (req, res ) {
  var organisationName = req.query.name;
  if(organisationName == undefined || organisationName == null){
    res.send("Name parameter missing");
  }else if (organisationName == ""){
    res.send("Name parameter cannot be empty");
  }else {
    //0 argument is for pagination. Returning first hundred organisations.
    Organisation.getRelatedOrganisations(organisationName, 0, function(result){
      res.send(result);
    });
  }
});
module.exports = router;