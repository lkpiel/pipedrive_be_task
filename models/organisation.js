var db = require('../dbconnection'); //reference of dbconnection.js

var Organisation = {
  addOrganisation: function (Organisation) {
    //Check if there are any organisations to add.
    if (Object.keys(Organisation.length > 0)) {
      for (var orgNr = 0; orgNr < Object.keys(Organisation).length; orgNr++) {
        var organisation = JSON.stringify(Organisation[orgNr].org_name);
        //Inserting new organisations to organisation table
        db.query("INSERT INTO organisation (name) VALUES (" + organisation + ")", function (error) {
          if (error) {
            //Every time when trying to add an organisation that is already added, an unique constraint makes it come here.
            console.log("Adding organisation failed: " + error);
          } else {
            console.log("Added organisation: " + organisation);
          }
        });
        if (Organisation[orgNr].daughters) {
          for (var childOrgNr = 0; childOrgNr < Object.keys(Organisation[orgNr].daughters).length; childOrgNr++) {
            var childOrganisation = JSON.stringify(Organisation[orgNr].daughters[childOrgNr].org_name);
            db.query("INSERT INTO relations (name, parent) VALUES (" + childOrganisation + ", " + organisation + ")", function (error) {
              if (error) {
                //There is an unique constraint to avoid duplicated relations. That will make the INSERT statement fail and come here.
                console.log("Adding relation failed: " + error);
              } else {
                console.log("Added relation: " + childOrganisation + " to organisation " + organisation);
              }
            });
          }
          //Recursive call to check for daughter organisatiokn daughters.
          this.addOrganisation(Organisation[orgNr].daughters);
        }
      }
    }
  }
  ,
  getRelatedOrganisations: function (name, limit, callback) {
        //I switch the columns to make the parent always the element we are interested in.
        db.query(`SELECT name, parent, 'noswitch' AS switched FROM relations AS org WHERE upper(name)= ?
        UNION ALL SELECT  parent AS name, name AS parent, 'switched' AS switched FROM relations WHERE upper(parent) IN (SELECT upper(parent) FROM relations WHERE upper(name)= ? ) AND upper(name) <> ?
        UNION ALL SELECT  parent AS name, name AS parent, 'switched' AS switched FROM relations AS org2 WHERE upper(parent) = ?
        ORDER by parent LIMIT ?, ?`, [name.toUpperCase(), name.toUpperCase(), name.toUpperCase(), name.toUpperCase(), limit * 100, limit + 100], function (err, rows) {
        var relatedOrganisations = rows;
        if (err) {
          callback("Error in retrieving the organisations" + err);
        } else {
          if (rows.length == 0) {
            callback("Could not find any results for your search!");
          } else {
            //Variable to store already added sister organisations to avoid duplicates 
            var addedSisters = [];
            var resultArray = [];
            for (var orgNr = 0; orgNr < Object.keys(relatedOrganisations).length; orgNr++) {
              if (relatedOrganisations[orgNr].name.toUpperCase() == name.toUpperCase()) {
                if (relatedOrganisations[orgNr].switched == "noswitch") {
                  resultArray.push({ "relationship_type": "parent", "org_name": relatedOrganisations[orgNr].parent });
                } else {
                  resultArray.push({ "relationship_type": "daughter", "org_name": relatedOrganisations[orgNr].parent });
                }
              } else {
                if (!addedSisters.includes(relatedOrganisations[orgNr].parent)) {
                  resultArray.push({ "relationship_type": "sister", "org_name": relatedOrganisations[orgNr].parent });
                  addedSisters.push(relatedOrganisations[orgNr].parent);
                }
              }
            }
            callback(resultArray);
          }
        }

      });


  }

};
module.exports = Organisation;